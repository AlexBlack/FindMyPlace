//
//  Location.swift
//  FindMyPlace
//
//  Created by Alex Black on 04.09.15.
//  Copyright (c) 2015 Alex Black. All rights reserved.
//

import Foundation
import CoreLocation

class Location  {
    static let sharedInstance = Location()
    
    let locationManager = CLLocationManager()
    
    init() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 2
        locationManager.requestWhenInUseAuthorization()
    }
    
    // Translates degress to degress, minutes and seconds
    func coordinateToString(coordinate: CLLocationCoordinate2D) -> (lat:String, lon: String) {
        
        let latitude = coordinate.latitude
        let longitude = coordinate.longitude
        
        var latSeconds = Int(latitude * 3600)
        let latDegrees = latSeconds / 3600
        latSeconds = abs(latSeconds % 3600)
        let latMinutes = latSeconds / 60
        latSeconds %= 60
        
        var longSeconds = Int(longitude * 3600)
        let longDegrees = longSeconds / 3600
        longSeconds = abs(longSeconds % 3600)
        let longMinutes = longSeconds / 60
        longSeconds %= 60
        
        let latString = String(format:"%d°%d'%d\"%@", abs(latDegrees), latMinutes, latSeconds, { return latDegrees >= 0 ? "N" : "S"}())
        let lonString = String(format: "%d°%d'%d\"%@", abs(longDegrees), longMinutes, longSeconds, {return longDegrees >= 0 ? "E" : "W"}())
        
        return (latString, lonString)
    }
}