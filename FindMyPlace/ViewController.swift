//
//  ViewController.swift
//  FindMyPlace
//
//  Created by Alex Black on 24.08.15.
//  Copyright (c) 2015 Alex Black. All rights reserved.
//

import UIKit
import GoogleMaps
import MessageUI

class ViewController: UIViewController, CLLocationManagerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var longitudeLbl: UILabel!
    @IBOutlet weak var latitudeLbl: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var mapTypeControl: UISegmentedControl!
    @IBOutlet weak var smsButton: UIButton!
    @IBOutlet weak var unitsButton: UIButton!
    
    let myLocation = Location.sharedInstance
    var currentLocation: CLLocation?
    let marker = GMSMarker()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var metric: Bool {
        get {
            if let value = defaults.valueForKey("metric") as? Bool {
                return value
            } else {
                defaults.setObject(false, forKey: "metric")
                defaults.synchronize()
                return false
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customization
        
        // Units
        if metric {
            unitsButton.setTitle(NSLocalizedString("METRIC", comment: "Metric system"), forState: .Normal)
        } else {
            unitsButton.setTitle(NSLocalizedString("IMPERIAL", comment: "imperial system"), forState: .Normal)
        }
        
        // Gradient
        let deviceScale = UIScreen.mainScreen().scale
        let gradient = CAGradientLayer()
        gradient.frame = CGRectMake(0, 0, self.gradientView.frame.size.width * deviceScale, self.gradientView.frame.size.height)
        gradient.colors = [UIColor.clearColor().CGColor, UIColor.whiteColor().CGColor]
        gradient.startPoint = CGPointMake(0.5, 0.0)
        gradient.endPoint = CGPointMake(0.5, 0.1)
        self.gradientView.layer.mask = gradient
        
        // SegmentedControl
        setSegControlAppearance()
        
        // Marker
        marker.icon = UIImage(named: "marker")
        marker.map = mapView
        
        // Location services
        myLocation.locationManager.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let location = currentLocation {
            if metric {
                accuracyLabel.text = NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(location.horizontalAccuracy)) " + NSLocalizedString("METERS", comment: "")
                unitsButton.setTitle(NSLocalizedString("METRIC", comment: ""), forState: .Normal)
            } else {
                accuracyLabel.text = NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(location.horizontalAccuracy * 3.2808399)) " + NSLocalizedString("FEET", comment: "")
                unitsButton.setTitle(NSLocalizedString("IMPERIAL", comment: ""), forState: .Normal)
            }
        }

    }
    
    @IBAction func mapTypeChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            mapView.mapType = kGMSTypeNormal
        case 1:
            mapView.mapType = kGMSTypeSatellite
        case 2:
            self.mapView.mapType = kGMSTypeHybrid
        default:
            mapView.mapType = mapView.mapType
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == .AuthorizedWhenInUse {
            myLocation.locationManager.startUpdatingLocation()

            mapView.myLocationEnabled = true
        }
    }
    
    // Чтобы камера центрировалась но не прыгала с точки на точку
    var flag4000 = false
    var flag150 = false
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last as CLLocation? {
            
            if -location.timestamp.timeIntervalSinceNow < 30.0 {
                
                // Если погрешностьт 4км, центрируем карту.
                if location.horizontalAccuracy >= 4000 || !flag4000 {
                    currentLocation = location
                    mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 12, bearing: 0, viewingAngle: 0)
                    if metric {
                        accuracyLabel.text =  NSLocalizedString("ACCURACY", comment: "") + "+/- \(Int(currentLocation!.horizontalAccuracy)) " + NSLocalizedString("METERS", comment: "")
                        unitsButton.setTitle(NSLocalizedString("METRIC", comment: ""), forState: .Normal)
                    } else {
                        accuracyLabel.text =  NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(currentLocation!.horizontalAccuracy * 3.2808399)) " + NSLocalizedString("FEET", comment: "")
                        unitsButton.setTitle(NSLocalizedString("IMPERIAL", comment: ""), forState: .Normal)
                    }
                    
                    flag4000 = true
                }
                
                
                // Если погрешность меньше 150м ставим маркер
                if location.horizontalAccuracy <= 150.0 && !flag150 {
                    
                    print(location.horizontalAccuracy)
                    
                    if !flag150 {
                        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
                        flag150 = true
                    }
                    
                    currentLocation = location
                    marker.position = location.coordinate
                    
                    smsButton.enabled = true
                    unitsButton.enabled = true
                    mapView.myLocationEnabled = false
                    
                    if metric {
                        accuracyLabel.text =  NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(currentLocation!.horizontalAccuracy)) " + NSLocalizedString("METERS", comment: "")
                        unitsButton.setTitle(NSLocalizedString("METRIC", comment: ""), forState: .Normal)
                    } else {
                        accuracyLabel.text =  NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(currentLocation!.horizontalAccuracy * 3.2808399)) " + NSLocalizedString("FEET", comment: "")
                        unitsButton.setTitle(NSLocalizedString("IMPERIAL", comment: ""), forState: .Normal)
                    }
                    
                    let textCoordinate = myLocation.coordinateToString(location.coordinate)
                    self.longitudeLbl.text = NSLocalizedString("LONG", comment: "") + textCoordinate.lon
                    self.latitudeLbl.text = NSLocalizedString("LAT", comment: "") + textCoordinate.lat
                    
                }
            } else {
                print("Your location is too old")
                return
            }
        }
    }
    
    @IBAction func smsButtonPressed(sender: UIButton) {
        let messageVC = MFMessageComposeViewController()
        
        let latitude: Double = currentLocation!.coordinate.latitude
        let longitude: Double = currentLocation!.coordinate.longitude
        
        messageVC.body = String(format: "http://maps.google.com/maps?q=%.5f,%.5f", latitude, longitude)
        print(String(format: "http://maps.google.com/maps?q=%.5f,%.5f", latitude, longitude))
        messageVC.messageComposeDelegate = self

        self.presentViewController(messageVC, animated: true, completion: nil)
    }
    
    // Закрытие окна Messages
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        if result == MessageComposeResultSent {
            let alert = UIAlertController(title: "", message: NSLocalizedString("MSG_SUCCESS", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "ok", style: .Default, handler: nil)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // Кнопка центрирования
    @IBAction func centerToLocation(sender: UIButton) {
        if let location = currentLocation {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0)
        }
    }
    
    @IBAction func unitsChanged(sender: UIButton) {
        if sender.currentTitle == NSLocalizedString("IMPERIAL", comment: "") {
            sender.setTitle(NSLocalizedString("METRIC", comment: ""), forState: .Normal)
            accuracyLabel.text = NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(currentLocation!.horizontalAccuracy)) " + NSLocalizedString("METERS", comment: "")
            defaults.setValue(true, forKey: "metric")
        } else {
            sender.setTitle(NSLocalizedString("IMPERIAL", comment: ""), forState: .Normal)
            accuracyLabel.text = NSLocalizedString("ACCURACY", comment: "") + " +/- \(Int(currentLocation!.horizontalAccuracy * 3.2808399)) " + NSLocalizedString("FEET", comment: "")
            defaults.setValue(false, forKey: "metric")
        }
        defaults.synchronize()
    }
    
    // Внешний вид SegmentedControl
    func setSegControlAppearance() {
        mapTypeControl.setDividerImage(UIImage(named: "LeftNRightN"), forLeftSegmentState: UIControlState.Normal, rightSegmentState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
        mapTypeControl.setDividerImage(UIImage(named: "LeftSRightN"), forLeftSegmentState: UIControlState.Selected, rightSegmentState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
        mapTypeControl.setDividerImage(UIImage(named: "LeftNRightS"), forLeftSegmentState: UIControlState.Normal, rightSegmentState: UIControlState.Selected, barMetrics: UIBarMetrics.Default)
        mapTypeControl.setBackgroundImage(UIImage(named: "LeftNRightN"), forState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
        mapTypeControl.setBackgroundImage(UIImage(named: "SelectedBody"), forState: UIControlState.Selected, barMetrics: UIBarMetrics.Default)
        mapTypeControl.setBackgroundImage(UIImage(named: "NormalBody"), forState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
    }
}
